var map = L.map('main_map').setView([-33.4400197,-70.6811611], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success:function(result){
        result.bicicletas.forEach(x => {
            L.marker(x.ubicacion).addTo(map)
            .bindPopup('id:'+x.id+', color:'+x.color+', modelo:'+x.modelo)
            .openPopup();
        });
    }
})

L.marker([-33.4400000,-70.6811611]).addTo(map)
    .bindPopup('Marcador 1.')
    .openPopup();

L.marker([-33.451000,-70.6811611]).addTo(map)
    .bindPopup('Marcador 2.')
    .openPopup();

L.marker([-33.4620020,-70.6811611]).addTo(map)
    .bindPopup('Marcador 3.')
    .openPopup();
