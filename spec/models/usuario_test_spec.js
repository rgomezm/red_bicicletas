var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

describe('Testing Usuarios', function(){
    beforeEach(function(done){
        var mongoDB = "mongodb+srv://cloud_mongodb_user01:naEKVq5ue7Ha2K3@cluster0.wmnuc.mongodb.net/test_red_bicicletas?retryWrites=true&w=majority";
        mongoose.connect(mongoDB, {  useNewUrlParser: true , useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console,'conection error'));
        db.once('open',function(){
            console.log('We are connected to test database!');

            done();
        });
        
    });

    afterEach(function(done){
        Reserva.deleteMany({},function(err,succes){
            if(err) console.log(err);
            Usuario.deleteMany({},function(err,succes){
                if(err) console.log(err);
                Bicicleta.deleteMany({},function(err,succes){
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un Usuario reserva una bici',()=>{
        it('debe existir la reserva', () =>{
            const usuario = new Usuario({nombre:'Ezequiel'});
            usuario.save();
            const bicicleta = new Bicicleta({code:1,color:"verde",modelo:"urbana"});
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err,reserva){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reserva[0].diasDeReserva()).toBe(2);
                    expect(reserva[0].bicicleta.code).toBe(1);
                    expect(reserva[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });     
        });
    });
});