var Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function(err,bicis){
        res.status(200).json({
            bicicletas: bicis
        });
    });
};

//SE CREAR UNA NUEVA BICICLETA
exports.bicicleta_create = (req, res) => {
  var bici = new Bicicleta({
    code: req.body.code,  
    color: req.body.color,
    modelo: req.body.modelo ,
    ubicacion : [req.body.lat, req.body.lng]
  });  
  //SE AGREGA LA NUEVA BICICLETA
  Bicicleta.add(bici,function(err){
      console.log(err)
  });
  //SE DEVUELVE AL JSON
  res.status(200).json({
    bicicleta: bici,
  });
};

exports.bicicleta_delete = function (req, res) {
  Bicicleta.removeByCode(req.body.code, function(){
    res.status(204).send();
  });
};

exports.bicicleta_update = function (req, res) {
  var bici = Bicicleta.findById(req.params.id);
  bici.code = req.body.code;
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.lat, req.body.lng];
  Bicicleta.update
  res.status(200).json({
    bicicleta: bici,
  });
};
