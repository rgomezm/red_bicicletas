var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = "mongodb+srv://cloud_mongodb_user01:naEKVq5ue7Ha2K3@cluster0.wmnuc.mongodb.net/test_red_bicicletas?retryWrites=true&w=majority";
        mongoose.connect(mongoDB, {  useNewUrlParser: true , useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console,'conection error'));
        db.once('open',function(){
            console.log('We are connected to test database!');
            done();
        });
        
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,succes){
            if(err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance',()=>{
        it('crea una instancia de una Bicicleta', () =>{
            var bici = Bicicleta.createInstance(8,"verde","urbana",[-34.5,-54.1]);
            expect(bici.code).toBe(8);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicileta.allBicis',() =>{
        it('comienza vacia',(done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add',()=> {
        it('agrega solo una bici', (done) =>{
            var aBici = new Bicicleta({code:1,color:"verde",modelo:"urbana"});
            Bicicleta.add(aBici,function(err,newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode',()=> {
        it('debe devolver una bici con cod3 1', (done) =>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                
                var aBici = Bicicleta.createInstance(1,"verde","urbana");
                Bicicleta.add(aBici,function(err, newBici){
                    if(err) console.log(err);
                    Bicicleta.findByCode(1,function(error, targetBici){
                        expect(targetBici.code).toBe(aBici.code);
                        expect(targetBici.color).toBe(aBici.color);
                        expect(targetBici.modelo).toBe(aBici.modelo);
                        done();
                    });
                });
                
            });
        });
    });
});

/*
beforeEach(() =>{
    Bicicleta.allBicis = [];
});
describe('Bicicleta.allBicis',() => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);    
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una bici', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var a = new Bicicleta(1, 'rojo', 'urbana', [-33.4400010,-70.6911611]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('debe devolver la bicic con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'azul', 'urbana', [-33.4410020,-70.682161]);
        var b = new Bicicleta(2, 'verde', 'urbana', [-33.4410020,-70.682161]);
        Bicicleta.add(a);
        Bicicleta.add(b);

        var targetBici = Bicicleta.findById(2);
        expect(targetBici.id).toBe(2);
        expect(targetBici.color).toBe(b.color);
        expect(targetBici.modelo).toBe(b.modelo);
    });
});

describe('Bicicleta.delete', () => {
    it('eliminamos una bici', () =>{
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis[0]).toBeNull;
    });
});

describe('Bicicleta.update', () => {
    it('modificamos una bici', () =>{
       
        var targetBici = Bicicleta.findById(2);
        targetBici.color = 'negro';
        Bicicleta.update(targetBici);
        expect(Bicicleta.allBicis[0].color).toBe(targetBici.color);
    });
});
*/