var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion : {
        type: [Number], index: {type:'2dsphere', sparse:true }
    }

});

BicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

BicicletaSchema.toString = function (){
    return 'code: ' + this.code + '| color: '+ this.color;
}

BicicletaSchema.statics.allBicis = function(cb){
    return this.find({},cb);
}

BicicletaSchema.statics.add = function (aBici, cb){
    this.create(aBici, cb);
}

BicicletaSchema.statics.findByCode = function (aCode, cb){
    return this.findOne({code: aCode}, cb);
}

BicicletaSchema.statics.removeByCode = function (aCode, cb){
    return this.deleteOne({code: aCode}, cb);
}

module.exports = mongoose.model('Bicicleta',BicicletaSchema);