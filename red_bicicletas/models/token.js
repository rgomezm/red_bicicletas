const moongose = require('mongoose');

const Schema = moongose.Schema;
const TokenSchema = new Schema({
    _userId:{ type:moongose.Schema.Types.ObjectId, require: true, ref:'Usuario' },
    token: {type: Date, required:true, default: Date.now, expires: 43200},
});

module.exports = moongose.model('Token', TokenSchema);