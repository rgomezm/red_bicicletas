const passport = require('passport');
const localStrategy= require('passport-local').Strategy;
const Usuario = require('../models/usuario');

passport.use(new localStrategy(
    function (email, password, done) {
        Usuario.findOne({ email: email },function(err,usuario){
            if(err) return done(err);
            if(!usuario) return done(null, false,  { message: 'Email no existe o incorrecto'});

            if (!usuario.VerificarPassword(password)) return done(null, false, { message: 'Contraseña no coincide' });
            
            return done(null, usuario);
        });
    }
));

passport.serializeUser(function (user, callback) {
    console.log('serializar')
    callback(null, user.id);
});

passport.deserializeUser(function (id, callback) {
    console.log('deserializar')
    console.log(id);
    Usuario.findById(id, function (err, usuario) {
        callback(err, usuario);
    });
});

module.exports = passport;

